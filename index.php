<?php

use mikevandiepen\utility\Validator;
use mikevandiepen\utility\RequestTransformer;

include ('src/RequestTransformer.php');
include ('src/Validator.php');
include ('src/Validation.php');
include ('src/ValidationInterface.php');
include ('src/Options/OptionBool.php');
include ('src/Options/OptionEmail.php');
include ('src/Options/OptionFloat.php');
include ('src/Options/OptionNumeric.php');
include ('src/Options/OptionRequired.php');
include ('src/Options/OptionString.php');
include ('src/Options/OptionUrl.php');

// Example of post array
$_POST = [
    'product_id'    => 5463,
    'product_name'  => 'Winter jacket',
    'color'         => 'red',
    'price'         => '10,00',
    'product_page'  => 'https://www.example-shop.com/men/jackets/winter_jackets'
];

$request = new RequestTransformer($_POST);

$results = Validator::validate([
    [
        $request->request('product_id'),
        'rules' => 'required|int'
    ],
    [
        $request->request('product_name'),
        'rules' => 'required|string'
    ],
    [
        $request->request('color'),
        'rules' => 'required|string'
    ],
    [
        $request->request('price'),
        'rules' => 'required|float'
    ],
    [
        $request->request('product_page'),
        'rules' => 'required|url'
    ],
]);