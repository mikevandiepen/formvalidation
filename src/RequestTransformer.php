<?php

namespace mikevandiepen\utility;

class RequestTransformer
{
    /**
     * The transformed data will be stored in here
     * @var array
     */
    private $request = array();

    /**
     * This constructor method transforms the post array to parsable data for the validation method
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $attribute => $value) {
            $this->request[$attribute] = [
                'name'  => $attribute,
                'value' => $value,
            ];
        }
    }

    /**
     * Requesting the formatted data from the $request property
     * @param string $attribute
     *
     * @return array
     */
    public function request(string $attribute): array
    {
        return $this->request[$attribute];
    }
}