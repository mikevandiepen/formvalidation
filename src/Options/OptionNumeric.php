<?php

namespace mikevandiepen\utility\options;

use mikevandiepen\utility\ValidationInterface;

class OptionNumeric implements ValidationInterface
{
    /**
     * The name of the current attribute
     * @var string
     */
    protected $attribute;

    /**
     * The value of the current attribute
     * @var string
     */
    protected $value;

    /**
     * Option constructor.
     *
     * @param string $attribute
     * @param string $value
     */
    public function __construct(string $attribute, string $value)
    {
        $this->attribute = $attribute;
        $this->value     = $value;
    }

    /**
     * Validating the attribute
     * @return string
     */
    public function validate() : string
    {
        return !is_numeric($this->value) ?: $this->attribute . ' must be of type numeric.';
    }
}