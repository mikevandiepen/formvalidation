<?php

namespace mikevandiepen\utility\options;

use mikevandiepen\utility\ValidationInterface;

class OptionFloat implements ValidationInterface
{
    /**
     * The name of the current attribute
     * @var string
     */
    protected $attribute;

    /**
     * The value of the current attribute
     * @var string
     */
    protected $value;

    /**
     * Option constructor.
     *
     * @param string $attribute
     * @param string $value
     */
    public function __construct(string $attribute, string $value)
    {
        $this->attribute = $attribute;
        $this->value     = $value;
    }

    /**
     * Validating the attribute
     * @return string
     */
    public function validate() : string
    {
        return !filter_var($this->value, FILTER_VALIDATE_FLOAT) ?: $this->attribute . ' must be of type numeric.';
    }
}