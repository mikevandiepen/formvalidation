<?php

namespace mikevandiepen\utility;

interface ValidationInterface
{
    /**
     * Option constructor.
     *
     * @param string $attribute
     * @param string $value
     */
    public function __construct(string $attribute, string $value);

    /**
     * Validating the attribute
     * @return string
     */
    public function validate(): string;
}