<?php

namespace mikevandiepen\utility;

use mikevandiepen\utility\options\OptionUrl;
use mikevandiepen\utility\options\OptionBool;
use mikevandiepen\utility\options\OptionEmail;
use mikevandiepen\utility\options\OptionFloat;
use mikevandiepen\utility\options\OptionString;
use mikevandiepen\utility\options\OptionNumeric;
use mikevandiepen\utility\options\OptionRequired;

class Validator
{
    /**
     * All error messages will be stored in here
     * @var array
     */
    private static $errors = array();

    /**
     * Validating all the values and applying all the assigned rules
     * @param array $request
     * @return array
     */
    public static function validate(array $request = array()): array
    {
        // Parsing through all the fields
        foreach ($request as $field) {

            // Transforming the rules to an array
            $rules = explode('|', $field['rules']);

            // Parsing through the rules and applying them to the request
            foreach ($rules as $rule) {
                if ($rule === 'required') {
                    $error = (new Validation(
                        new OptionRequired($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'string') {
                    $error = (new Validation(
                        new OptionString($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'numeric') {
                    $error = (new Validation(
                        new OptionNumeric($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'bool') {
                    $error = (new Validation(
                        new OptionBool($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'float') {
                    $error = (new Validation(
                        new OptionFloat($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'email') {
                    $error = (new Validation(
                        new OptionEmail($field['name'], $field['value']))
                    )->validate();

                } elseif ($rule === 'url') {
                    $error = (new Validation(
                        new OptionUrl($field['name'], $field['value']))
                    )->validate();

                //------------------------------------------------------------------------------------------------------
                // More validation rules can be added here
                //------------------------------------------------------------------------------------------------------
                //
                //  EXAMPLE:
                //
                //  } elseif ($rule === 'rule') {
                //      $error = (new Validation(
                //          new OptionRule($field['name'], $field['value']))
                //      )->validate();
                //  }
                //
                //------------------------------------------------------------------------------------------------------

                } else {
                    $error = [];
                }

                self::$errors[$field['name']]['errors'][] = $error;
            }
        }

        return self::$errors;
    }
}